<?php

class FindS {

    private $data = [];
    private $decisionAttributIndex = null;
    private $hypothesis = [];
    
    public function __construct() {
        $this->data = json_decode(file_get_contents(__DIR__."/data.json"), true);
        $this->decisionAttributIndex = $this->data["A"][$this->data["decision"]];
        //$this->hypothesis = count($this->data["A"])-1;
        return $this;
    }

    public function getHypothesis() {
        foreach ($this->data["data"] as $record) {
            if (!$record[(int)$this->decisionAttributIndex]) continue;
            $this->compareWithHypothesis($record);
        }
        return $this->hypothesis;
    }

    private function compareWithHypothesis($record) {
        if (count($this->hypothesis)===0) $this->hypothesis = $record;
        else {
            foreach ($record as $key => $attribute) {
                if ($attribute!==$this->hypothesis[$key]) $this->hypothesis[$key] = "?";
            }
        }
    }
}

$find = new FindS();
var_dump($find->getHypothesis());
